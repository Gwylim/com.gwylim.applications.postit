# Post It Note Application #

### Functionality present: ###

* User signup/login

* Post It Note:

* * Creation

* * List

* * Edit

* * Delete

* Following:

* * Add Follow

* * Remove Follow

* * Display Followed user postits


### Suggested extensions: ###

* REST API layer for flexibility, e.g. mobile client applications.

* Sharing Postit Notes with specified others

* Making notes public

* User editing features

* * Username is not used as id so it can be easily changed.


### Production essential omissions: ###

* Internationalisation

* Better Layout of Sign In / Up pages.

* Unit testing of the Controller(s).


## How do I get set up? ##

This project uses gradle.
Build with:
gradle build

Then launch with:
java -jar com.gwylim.applications.postit.web.server/build/libs/Gwylim-Postit-Server-0.1.0.jar

The project is currently configured to use a Hibernate in memory database to remove any infrastructure requirements. This can be changed by editing the hibernate.cfg.xml and application.properties files. The PostgreSQL configuration used during development is present for reference.

### Eclipse import: ###

1. Import... -> Clone Existing Mercurial Repository

2. URL: https://bitbucket.org/Gwylim/com.gwylim.applications.postit

3. Right click on project... Configure -> Convert to Gradle Project

4. Right click on build.gradle -> Run as -> Gradle Build...

5. In the box labelled "Type tasks in the editor below." enter "eclipse"... Run

6. Right click... Import...

7. Gradle Project

8. Browse to the root of the project and press "Build Model"

9. Select all but the root project (it has the grey icon indicating it is already present in the workspace)

10. Main class can be found here: com.gwylim.applications.postit.web.server/src/main/java/com/gwylim/applications/postit/web/server/Main.java



### Command line: ###

Running the following command in com.gwylim.applications.postit.web.server directory will create an executable jar in build/dist:

```
#!bash

gradle duild
```

Which can be run with:

```
#!bash

java -jar build/libs/Gwylim-Postit-Server-0.1.0.jar
```
this will use an in-memory database.

The webapp can be accessed at:
http://localhost:8080