package com.gwylim.applications.postit.web.server.basic.formData;

/**
 * Data for creating a new postit or editing one.
 * See CreatePostit.html and EditPostit.html.
 */
public class PostitData extends CheckedData {

  private String _title;
  private String _body;

  private String _titleError;
  private String _bodyError;

  /**
   * Create a new postit.
   * @param title The title of the new postit.
   * @param body  The body test of the new postit.
   */
  public PostitData(final String title, final String body) {
    _title = title;
    _body = body;
  }

  /**
   * For reflective construction.
   */
  public PostitData() { }

  public String getTitle() { return _title; }
  /** Set the title. */
  public void setTitle(final String title) {
    _title = title;
    _titleError = check(title);
  }

  public String getBody() { return _body; }
  /** Set the body. */
  public void setBody(final String body) {
    _body = body;
    _bodyError = check(body);
  }

  @Override
  public boolean hasErrors() { return _bodyError != null || _titleError != null; }

  public String getBodyError() { return _bodyError; }
  public void setBodyError(final String bodyError) { _bodyError = bodyError; }

  public String getTitleError() { return _titleError; }
  public void setTitleError(final String titleError) { _titleError = titleError; }
}
