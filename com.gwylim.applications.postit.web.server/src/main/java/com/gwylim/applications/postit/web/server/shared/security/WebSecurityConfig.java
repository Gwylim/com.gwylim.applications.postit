package com.gwylim.applications.postit.web.server.shared.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuration of Spring security.
 */
@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .csrf().disable() // To allow the REST DELETE/PUT functionality to work... this is not optimal.
                              // http://stackoverflow.com/questions/19468209/spring-security-403-error

            .authorizeRequests()
                .antMatchers("/SignUp").permitAll()
                .antMatchers("/**.css").permitAll()
                .antMatchers("/**.png").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/SignIn")
                .permitAll()
                .and()
            .logout()
                .permitAll();
    }

    /**
     * Configure the authentication manager.
     */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth, final UserDetailsService userDetailsService, final PasswordEncoder passwordEncoder) throws Exception {
      auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    /**
     * Get the {@link PasswordEncoder} used for user accounts.
     * @return A {@link PasswordEncoder}.
     */
    @Bean
    public static PasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
    }
}