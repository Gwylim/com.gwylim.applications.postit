package com.gwylim.applications.postit.dao.postit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.gwylim.applications.postit.dao.user.HibernateUser;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;

/**
 * A postit stored in Hibernate ORM.
 *
 * @author Gwylim
 */
@Entity
@Table(name = "postits")
public class HibernatePostit implements Postit {

  private PostitID _id = new PostitID();

  private HibernateUser _author;

  private String _title;

  private String _body;

  private Date _created;

  /***/
  @SuppressWarnings("unused")
  private HibernatePostit() { } // For deserialisation

  /**
   * Create a new postit.
   */
  public HibernatePostit(final String title, final String body, final User user) {
    _title = title;
    _body = body;
    setHibernateAuthor((HibernateUser) user);
    setCreated(new Date());
  }

  @Id
  @Column(name = "id")
  private String getId() { return _id.toDB(); }
  @SuppressWarnings("unused")
  private void setId(final String id) { _id = new PostitID(id); }

  /**
   * @see com.gwylim.applications.postit.web.server.model.Positit#getPostitID()
   */
  @Override
  @Transient
  public PostitID getPostitID() { return _id; }


  @JoinColumn(name = "author")
  @ManyToOne() // Do not cascade as this is a leaf.
  private HibernateUser getHibernateAuthor() { return _author; }
  private void setHibernateAuthor(final HibernateUser author) { _author = author; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.Postit#getAuthor()
   */
  @Override
  @Transient
  public User getAuthor() { return _author; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.Positit#getTitle()
   */
  @Override
  @Column(name = "title", nullable = false)
  public String getTitle() { return _title; }
  void setTitle(final String title) { _title = title; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.Positit#getBody()
   */
  @Override
  @Column(name = "body", nullable = false)
  public String getBody() { return _body; }
  void setBody(final String body) { _body = body; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.Positit #getCreated()
   */
  @Type(type = "date")
  @Override
  @Column(name = "created", nullable = false)
  public Date getCreated() { return new Date(_created.getTime()); }
  private void setCreated(final Date created) { _created = created; }


  @Override
  public String toString() {
    return "Postit [title=" + _title + ", body=" + _body + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (_id == null ? 0 : _id.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) { return true; }
    if (obj == null) { return false; }
    if (getClass() != obj.getClass()) { return false; }
    final HibernatePostit other = (HibernatePostit) obj;
    if (_id == null) {
      if (other._id != null) { return false; }
    }
    else if (!_id.equals(other._id)) {
      return false;
    }
    return true;
  }
}
