package com.gwylim.applications.postit.web.server.basic;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserService;

/**
 * Spring MVC controller for the webapp.
 *
 * This class handles  postit display/edit/create.
 *
 * @author Gwylim
 *
 */
@Controller
public class UserController {

  private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

  private final UserService _userService;

  /***/
  @Autowired
  public UserController(final UserService userService) {
    LOG.debug("Created UserController.");
    _userService = userService;
  }

   /**
   *  POST handler for a request to follow another user.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(value = "/follow", method = RequestMethod.POST)
  public String follow(final Model model, @ModelAttribute(value = "friendUsername") final String friendName, final Principal principal) throws NoSuchUserException {
    final User user = _userService.getUser(principal.getName());
    LOG.debug(user + " follow: " + friendName);

    try {
      final User friend = _userService.getUser(friendName);
      _userService.follow(user, friend);
      return "redirect:/static";
    }
    catch (final NoSuchUserException e) {
      LOG.warn(friendName + " does not exist.");
      return "redirect:/static";
    }
  }

  /**
   * Request to unfollow a user.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(value = "/unfollow", method = RequestMethod.POST)
  public String unfollow(final Model model, @RequestParam(value = "id", required = true) final String idString, final Principal principal) throws NoSuchUserException {
    final User user = _userService.getUser(principal.getName());

    try {
      final User followed = _userService.getUser(new UserID(idString));
      LOG.debug(user + " unfollow: " + followed.getUsername());
      _userService.unfollow(user, followed);
    }
    catch (final NoSuchUserException e) {
      LOG.error("Attempt to follow user with ID: '" + idString + "' which does not exist. There should be a contraint in the database to precude this scenario.");
      throw e;
    }

    return "redirect:/static";
  }
}
