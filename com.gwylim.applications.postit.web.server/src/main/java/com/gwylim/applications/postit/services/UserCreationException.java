package com.gwylim.applications.postit.services;

import org.hibernate.HibernateException;


/**
 * Exception thrown when a user could not be created.
 *
 * @author Gwylim
 */
@SuppressWarnings("serial")
public class UserCreationException extends Exception {
  /***/
  public UserCreationException(final String message) {
    super(message);
  }

  /***/
  public UserCreationException(final HibernateException e) {
    super(e);
  }
}
