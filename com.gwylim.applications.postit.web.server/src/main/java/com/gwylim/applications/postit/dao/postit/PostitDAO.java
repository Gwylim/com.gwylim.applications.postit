package com.gwylim.applications.postit.dao.postit;

import java.util.List;

import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;

/**
 * Data access object for performing operations on {@link HibernatePostit}s.
 */
public interface PostitDAO {

  /**
   * Get a postit by id.
   * @param id  The id of the Postit to get.
   * @return    The postit or <code>null</code> if no postit exists with that ID.
   */
  Postit getPostit(PostitID id);

  /**
   * Update the database representation of the supplied postit.
   * @param postit The Postit to delete from the database.
   * @param title  The new title.
   * @param body   The new body.
   * @return       The updated Postit.
   */
  Postit update(Postit postit, String title, String body);

  /**
   * Delete a {@link Postit}.
   */
  void delete(Postit postit);

  /**
   * Get the {@link Postit}s from users that the fiven user follows.
   */
  List<Postit> getFollowedPostits(User user);

  /**
   * Save a new {@link Postit} to the database.
   */
  void save(Postit postit);
}
