package com.gwylim.applications.postit.web.server.basic;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.NewPostitService;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.PostitService;
import com.gwylim.applications.postit.services.UserService;
import com.gwylim.applications.postit.web.server.basic.formData.PostitData;

/**
 * Spring MVC controller for the web app.
 *
 * This class handles user login/out and creation.
 *
 * @author Gwylim
 *
 */
@Controller
public class PostitController {

  private static final Logger LOG = LoggerFactory.getLogger(PostitController.class);

  private final NewPostitService _newPostitService;
  private final UserService _userService;
  private final PostitService _postitService;

  /**
   * Construct.
   */
  @Autowired
  public PostitController(final UserService userService, final PostitService postitService, final NewPostitService newPostitService) {
    LOG.debug("Created PostitController.");
    _userService = userService;
    _postitService = postitService;
    _newPostitService = newPostitService;
  }

  /**
   *  Provides the page for a user to create a new postit.
   */
  @RequestMapping(value = "/CreatePostit", method = RequestMethod.GET)
  public String createPostitGet(final Model model, final Principal principal) {
    final String username = principal.getName();

    LOG.debug("New postit creation started by " + username);
    model.addAttribute("data", new PostitData());
    return "CreatePostit";
  }

  /**
   * Receives POSTs from the CreatePostit page and creates a new postit.
   * The data from the POST is validated first, if there are error the user is sent back to the CreatePostit page with their original data and error messages.
   * @throws NoSuchUserException When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(value = "/CreatePostit", method = RequestMethod.POST)
  public String createPostitPost(@ModelAttribute(value = "data") final PostitData data, final Model model, final Principal principal) throws NoSuchUserException {
    final String username = principal.getName();

    final User user = _userService.getUser(username);

    if (data.hasErrors()) {
      model.addAttribute("data", data);
      return "CreatePostit";
    }

    Postit newPostit;
    try {
      newPostit = _newPostitService.newPostit(data.getTitle(), data.getBody(), user);
      LOG.debug("New postit '" + newPostit + "' created by " + user + " title " + data.getTitle());
      return "redirect:/static";
    }
    catch (final NoSuchUserException e) {
      LOG.error("Attempt to creste a new postit by " + user + " which nolonger exists in the database. Title " + data.getTitle(), e);
      return "redirect:/logout";
    }
  }




  /**
   * Provides a page for the editing of an existing postit.
   * The model contains the data from the current postit.
   * @param idString The id of the postit to edit.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(value = "/EditPostit", method = RequestMethod.GET)
  public String editPostitGet(@RequestParam(value = "id", required = true) final String idString, final Model model, final Principal principal) throws NoSuchUserException {
    final String username = principal.getName();
    final PostitID id = new PostitID(idString);

    final Postit postit = _postitService.getPostit(id);
    final User user = _userService.getUser(username);

    if (!user.getPostits().contains(postit)) {
      LOG.warn("User " + user + " attempting EditPostit GET for " + postit + " that is not theirs, redirecting to SignIn.");
      return "redirect:SignIn";
    }

    LOG.debug("Edit postit " + id + "  started by " + user);
    final PostitData postitData = new PostitData(postit.getTitle(), postit.getBody());
    model.addAttribute("data", postitData);
    model.addAttribute("id", id);
    return "EditPostit";
  }

  /**
   * Receives POSTs from the EditPostit page and creates a postit from it.
   * The data is first validated and if there is an error the user is sent back to the EditPostit page with their data and error messages.
   * @param idString The id of the postit to edit.
   * @param data The PostitData.
   * @param model The model.
   * @param principal The principal.
   * @return The edit view on failure or / on success, non-logged in users are redirected to signin.
   * @throws NoSuchUserException When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(value = "/EditPostit", method = RequestMethod.POST)
  public String editPostitPost(@RequestParam(value = "id", required = true) final String idString, @ModelAttribute(value = "data") final PostitData data, final Model model, final Principal principal) throws NoSuchUserException {
    final String username = principal.getName();
    final PostitID id = new PostitID(idString);

    final User user = _userService.getUser(username);

    if (data.hasErrors()) {
      model.addAttribute("data", data);
      model.addAttribute("id", id);
      return "EditPostit";
    }

    final Postit postit = _postitService.getPostit(id);

    if (!user.getPostits().contains(postit)) {
      LOG.warn("User " + user + " attempting EditPostit POST for " + postit + " that is not theirs, redirecting to SignIn.");
      return "redirect:SignIn";
    }

    LOG.debug("Postit " + id + " '" + postit.getTitle() + "' edited by " + user + " new title: '" + data.getTitle() + "'.");
    _postitService.update(postit, data.getTitle(), data.getBody());
    return "redirect:/static";
  }


  /**
   *  Deletes the specified postit.
   *  If the signed in user does not own the specified postit the postit is not deleted and the user is redirected to the FrontPage.
   *  @param idString The ID of the postit to be deleted.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(value = "/DeletePostit", method = RequestMethod.POST)
  public String deletePostitPost(@RequestParam(value = "id", required = true) final String idString, final Model model, final Principal principal) throws NoSuchUserException {
    final String username = principal.getName();
    final PostitID id = new PostitID(idString);

    final Postit postit = _postitService.getPostit(id);
    final User user = _userService.getUser(username);

    if (!user.getPostits().contains(postit)) {
      LOG.warn("User " + user + " attempting EditPostit POST for " + postit + " that is not theirs, redirecting to SignIn.");
      return "redirect:SignIn";
    }

    if (postit == null) {
      LOG.warn("User " + user + " attempted to delete non existant postit: " + id);
      return "redirect:/static";
    }
    else {
      _postitService.delete(postit);
      return "redirect:/static";
    }
  }
}
