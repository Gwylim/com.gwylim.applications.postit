package com.gwylim.applications.postit.web.server.shared;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.UserCreationException;
import com.gwylim.applications.postit.services.UserService;
import com.gwylim.applications.postit.web.server.shared.formData.SignUpData;

/**
 * Spring MVC controller for the webapp.
 *
 * This class handles user signin/out.
 *
 * @author Gwylim
 *
 */
@Controller
public class SignInOutController {

  private static final Logger LOG = LoggerFactory.getLogger(SignInOutController.class);

  private final UserService _userService;

  /***/
  @Autowired
  public SignInOutController(final UserService userService) {
    LOG.debug("Created UserController.");
    _userService = userService;
  }

  /**
   * This mapping provides the sign in page for the user to fill out.
   */
  @RequestMapping(value = "/SignIn", method = RequestMethod.GET)
  public String signInPageGet() { return "SignIn"; }

  /**
   * Provides the page for a user to sign up for the service.
   */
  @RequestMapping(value = "/SignUp", method = RequestMethod.GET)
  public String signUpPageGet(final Model model) {
    model.addAttribute("data", new SignUpData());
    return "SignUp";
  }

  /**
   *  Receives the POST from the sign up page.
   *  Will add the user to the database to create the user, or if that fails send them back to the sign in page with an error message.
   */
  @RequestMapping(value = "/SignUp", method = RequestMethod.POST)
  public String signUpPagePost(@ModelAttribute(value = "data") final SignUpData data, final Model model) {
    final String username = data.getUsername();

    if (data.hasErrors()) { // If there are errors that stop us creating the user...
      if (_userService.existsByName(username)) { // If the user already exists add it to the errors.
        data.setUsernameError(UserService.createUserExistsMessage(username));
      }
      model.addAttribute("data", data);
      return "SignUp";
    }

    try {
      final UserID user = _userService.newUser(username, data.getPassword());
      if (user != null) {
        LOG.info("New user created: " + data);
        return "redirect:/";
      }
      else {
        LOG.error("Error creating user: " + username);
        data.setUsernameError("There was an error creating your account, please try again later.");
        model.addAttribute("data", data);
        return "SignUp";
      }
    }
    catch (final UserCreationException e) {
      LOG.info("Attempt to create user that already exists: " + username);
      data.setUsernameError(UserService.createUserExistsMessage(username));
      model.addAttribute("data", data);
      return "SignUp";
    }
  }
}
