package com.gwylim.applications.postit.services;

import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;

/**
 * Data access object for performing operations on HibernateUsers.
 */
public interface UserService {

  /**
   * Create a new user in the system.
   * @param username      The name of the user to create.
   * @param password  The <strong>unencoded</strong> user password.
   * @return      The newly created user.
   * @throws      UserCreationException When the user could not be created.
   */
  UserID newUser(String username, final String password) throws UserCreationException;

  /**
   * Create the error message for existing user.
   * @param username The user that we are checking for.
   * @return The error message for existing user.
   */
  static String createUserExistsMessage(final String username) {
    return "User with name '" + username + "' already exists.";
  }

  /**
   * Update the username for the given user.
   * @param user        The user to update.
   * @param newUsername The new username.
   */
  void updateUsername(User user, String newUsername);

  /**
   * Get a User by username from the database.
   * @param username  The username of the required HibernateUser.
   * @return The user that has the given user name.
   * @throws NoSuchUserException  When there is no user with the requested username.
   */
  User getUser(String username) throws NoSuchUserException;

  /**
   * Get a user from the database by ID.
   * @param id  The Id to look for in the database.
   * @return The user for the given ID.
   * @throws NoSuchUserException If there is no user with the specified ID.
   */
  User getUser(UserID id) throws NoSuchUserException;


  /**
   * Is there a user with the given name in the database already?
   * @param username The username to check for.
   * @return <code>true</code> if the user already exists.
   */
  boolean existsByName(String username);

  /**
   * Delete the supplied user from the database.
   * @param user The user to delete.
   */
  void delete(User user);

  /**
   * Delete the supplied user from the database by ID.
   * @param userID The user to delete by ID.
   */
  void delete(UserID userID);

  /**
   * Make two {@link User}s friends.
   * @param userA New Friend A.
   * @param userB New Friend B.
   */
  void makeFriends(User userA, User userB);

  /**
   * Make follower follow followed.
   */
  void follow(final User follower, final User followed);

  /**
   * Make follower nolonger follow followed.
   */
  void unfollow(final User follower, final User followed);
}
