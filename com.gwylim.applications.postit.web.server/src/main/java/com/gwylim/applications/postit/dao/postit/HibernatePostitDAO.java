package com.gwylim.applications.postit.dao.postit;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gwylim.applications.postit.dao.HibernateDAO;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;

/**
 * @see PostitDAO.
 */
@Service
public class HibernatePostitDAO extends HibernateDAO implements PostitDAO {

  /**
   * Create a {@link HibernatePostitDAO} for testing.
   * @param sessionFactory The {@link SessionFactory} to use for testing.
   */
  @Autowired
  public HibernatePostitDAO(final SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public Postit getPostit(final PostitID id) {
    return (Postit) getSession().get(HibernatePostit.class, id.toDB());
  }


  @Override
  public Postit update(final Postit postit, final String title, final String body) {
    final HibernatePostit hibernatePostit = (HibernatePostit) postit;
    hibernatePostit.setTitle(title);
    hibernatePostit.setBody(body);
    return (HibernatePostit) getSession().merge(hibernatePostit);
  }

  @Override
  public void delete(final Postit postit) {
    getSession().delete(postit);
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<Postit> getFollowedPostits(final User user) {
    final Query query = getSession().createQuery("SELECT p FROM HibernateUser u "
                                                        + "INNER JOIN u.hibernateFollowing f "
                                                        + "INNER JOIN f.hibernatePostits p "
                                                        + "WHERE u = :user "
                                                        + "ORDER BY p.created");
    query.setEntity("user", user);

    return query.list();
  }

  @Override
  public void save(final Postit postit) {
    getSession().save(postit);
  }
}
