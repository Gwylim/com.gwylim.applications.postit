package com.gwylim.applications.postit.services;

import java.util.List;

import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;

/**
 * Data access object for performing operations on HibernatePostit.
 */
public interface PostitService {

  /**
   * Get a postit by id.
   * @param id  The id of the Postit to get.
   * @return    The postit or <code>null</code> if no postit exists with that ID.
   *
   * TODO: Throw exception when find fails.
   */
  Postit getPostit(PostitID id);

  /**
   * Update the database representation of the supplied postit.
   * @param postit The Postit to delete from the database.
   * @param title The new title for the postit.
   * @param body  The new body for the postit.
   * @return       The updated Postit.
   */
  Postit update(Postit postit, String title, String body);

  /**
   * Delete a postit.
   */
  void delete(Postit postit);

  /**
   * Get the postits for the users that the given user follows.
   */
  List<Postit> getFollowedPostits(User user);
}
