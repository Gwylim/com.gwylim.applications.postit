package com.gwylim.applications.postit.services.impl;


import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gwylim.applications.postit.dao.user.UserDAO;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;
import com.gwylim.applications.postit.services.UserService;

/**
 * Data access object for performing operations on {@link User}s.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {UserCreationException.class})
public class UserServiceImpl implements UserService, UserDetailsService {
  private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

  private final UserDAO _userDAO;
  private final PasswordEncoder _passwordEncoder;

  /***/
  @Autowired
  public UserServiceImpl(final UserDAO userDAO, final PasswordEncoder passwordEncoder) {
    _userDAO = userDAO;
    _passwordEncoder = passwordEncoder;
  }


  /**
   * Create a new {@link User} in the system.
   * @param name  The name of the user to create.
   * @return      The newly created user, never <code>null</code>.
   * @throws      UserCreationException When the {@link User} could not be created.
   */
  @Override
  public UserID newUser(final String username, final String password) throws UserCreationException {
    try {
      _userDAO.getUser(username);
      throw new UserCreationException(UserService.createUserExistsMessage(username)); // If we can get the username we cannot create a new one.
    }
    catch (final NoSuchUserException e) {
      final UserID newUser = _userDAO.newUser(username, _passwordEncoder.encode(password)); // TODO: This will throw a constraint violation exception.
      if (newUser == null) {
        LOG.error("Failed to create a user for an unknown reason.");
        throw new UserCreationException("User could not be created.");
      }
      else {
        return newUser;
      }
    }
  }


  /**
   * Get a {@link User} by username from the database.
   * @param username  The username of the required {@link User}.
   * @return          The {@link User} that has the given username.
   * @throws NoSuchUserException When there is no user with the requested username.
   */
  @Override
  public User getUser(final String username) throws NoSuchUserException {
    return _userDAO.getUser(username);
  }


  /**
   * Get a {@link User} from the database by ID.
   * @param id  The Id to look for in the database.
   * @return The user for the given ID.
   * @throws NoSuchUserException If there is no user with the specified ID.
   */
  @Override
  public User getUser(final UserID id) throws NoSuchUserException {
    final User user = _userDAO.getUser(id);

    if (user != null) {
      return user;
    }
    else {
      throw new NoSuchUserException("User ID " + id + " does not exist.");
    }
  }


  /**
   * Does a user with the given username exist.
   * @param username  The username to look for.
   * @return  <code>true</code> if the user exists. <code>false</code> otherwise.
   */
  @Override
  public boolean existsByName(final String username) {
    try {
      getUser(username);
      return true;
    }
    catch (final NoSuchUserException e) {
      return false;
    }
  }


  /**
   * Save updates to this {@link User} to the database.
   * @param user The {@link User} to be saved.
   */
  @Override
  public void updateUsername(final User user, final String newUsername) {
    _userDAO.updateUsername(user, newUsername);
  }


  /**
   * Delete the supplied user from the database.
   * @param user The {@link User} to delete.
   */
  @Override
  public void delete(final User user) {
    _userDAO.delete(user);
  }


  /**
   * Delete the supplied user from the database by ID.
   * @param userID The {@link User} to delete by ID.
   */
  @Override
  public void delete(final UserID userID) {
    _userDAO.delete(userID);
  }


  /**
   * Enter the given users into a mutual following relationship.
   */
  @Override
  public void makeFriends(final User userA, final User userB) {
    _userDAO.follow(userA, userB);
    _userDAO.follow(userB, userA);
  }


  @Override
  public void follow(final User follower, final User followed) {
    if (follower == null || followed == null || follower.equals(followed)) {
      LOG.error("Attempted to make " + follower + " follow " + followed + ".");
      return;
    }
    _userDAO.follow(follower, followed);
  }


  @Override
  public void unfollow(final User follower, final User followed) {
    if (follower == null || followed == null || follower.equals(followed)) {
      LOG.error("Attempted to make " + follower + " unfollow " + followed + ".");
      return;
    }
    _userDAO.unfollow(follower, followed);
  }


  /**
   * @see UserDetailsService#loadUserByUsername(String)
   */
  @Override
  @SuppressWarnings("serial")
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    try {
      final User user = _userDAO.getUser(username);
      LOG.debug("Sucessfully returning user requested by name: " + username);

      return new UserDetails() {
        @Override public boolean isEnabled() { return true; }
        @Override public boolean isCredentialsNonExpired() { return true; }
        @Override public boolean isAccountNonLocked() { return true; }
        @Override public boolean isAccountNonExpired() { return true; }
        @Override public String getUsername() { return username; }
        @Override public String getPassword() { return user.getPassword(); }
        @Override public Collection<? extends GrantedAuthority> getAuthorities() { return Collections.emptyList(); }
      };
    }
    catch (final NoSuchUserException e) {
        LOG.info("User requested by name '" + username + "' not found");
        throw new UsernameNotFoundException("No such user: " + username);
    }
  }
}
