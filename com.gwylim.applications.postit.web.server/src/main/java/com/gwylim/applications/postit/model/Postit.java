package com.gwylim.applications.postit.model;

import java.util.Date;

/**
 * A postit.
 *
 * @author Gwylim
 */
public interface Postit {

  /**
   * Get the PostitID of this postit.
   */
  PostitID getPostitID();


  /**
   * Get the author User of this postit.
   */
  User getAuthor();

  /**
   * Get the title of this postit.
   */
  String getTitle();

  /**
   * Get the body text of this postit.
   */
  String getBody();

  /**
   * Get the Date that this postit was created.
   */
  Date getCreated();
}
