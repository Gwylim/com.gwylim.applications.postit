package com.gwylim.applications.postit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot entry point.
 */
@SpringBootApplication
public class Main {
  /**
   * Spring boot entry point.
   */
  public static void main(final String[] args) {
    SpringApplication.run(Main.class, args);
  }
}
