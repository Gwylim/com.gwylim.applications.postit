package com.gwylim.applications.postit.dao.ids;

/**
 * An ID that can be used in the database and for Object identity.
 * Sub classes must be immutable wrt the returned value of toDB.
 *
 * See:
 * http://www.onjava.com/pub/a/onjava/2006/09/13/dont-let-hibernate-steal-your-identity.html
 *
 * @author Gwylim
 *
 * @param <T> The database friendly type to convert the ID to/from. It is required that there exists a constructor in concrete sub types of this class that can create IDs from T.
 */
public abstract class ID<T> {

  /**
   * Convert this ID to a database format.
   * Sub classes must be immutable wrt the returned value of toDB.
   * @return The database friendly ID, this must not mutate between calls.
   */
  public abstract T toDB();

  @Override
  public final boolean equals(final Object obj) {
    if (obj != null && obj.getClass().equals(getClass())) {
      return toDB().equals(getClass().cast(obj).toDB());
    }
    return false;
  }

  @Override
  public final int hashCode() { return toDB().hashCode(); }
}
