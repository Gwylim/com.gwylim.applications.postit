package com.gwylim.applications.postit.web.server.REST.model;

import com.gwylim.applications.postit.model.Postit;

/**
 * Version of a Postit that can be converted to a JSON object for REST responses.
 *
 * @author Gwylim
 */
public class RESTPostit {
  private String _title;
  private String _body;
  private String _id;

  /** For deserialisation. */
  private RESTPostit() { }

  /**
   * Create a RESTPostit from a {@link Postit}.
   */
  public RESTPostit(final Postit postit) {
    this();
    setTitle(postit.getTitle());
    setBody(postit.getBody());
    setId(postit.getPostitID().toDB());
  }

  public String getTitle() { return _title; }
  public String getBody() { return _body; }
  public String getId() { return _id; }

  private void setTitle(final String title) { _title = title; }
  private void setBody(final String body) { _body = body; }
  private void setId(final String id) { _id = id; }
}
