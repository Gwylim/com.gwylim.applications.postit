package com.gwylim.applications.postit.model;

import com.gwylim.applications.postit.dao.ids.UUIDID;

/**
 * Typed extension of ID for Postit Objects.
 *
 * @author Gwylim
 */
public class PostitID extends UUIDID {
  public PostitID() { super(); }
  public PostitID(final String id) { super(id); }
}
