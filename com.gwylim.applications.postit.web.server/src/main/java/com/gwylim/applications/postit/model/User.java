package com.gwylim.applications.postit.model;

import java.util.List;
import java.util.Set;

/**
 * A {@link User} of the application.
 *
 * @author Gwylim
 */
public interface User {

  /**
   * Get the {@link UserID} for this {@link User}.
   * @return This user's ID.
   */
  UserID getUserID();

  /**
   * Get this user's username.
   * @return The username of this {@link User}.
   */
  String getUsername();

  /**
   * Get the <strong>encoded</strong> password for this {@link User}.
   * @return The encoded password.
   */
  String getPassword();

  /**
   * Set the <strong>encoded</strong> password for this {@link User}.
   * @param password the new encoded password for this {@link User}.
   */
  void setPassword(String password);

  /**
   * Get the {@link Postit} owned by this {@link User}.
   * @return A {@link List} of {@link Postit}s owned by this {@link User}, this {@link List} is NOT modifiable.
   */
  List<Postit> getPostits();

  /**
   * Get the friends of this {@link User).
   * @return  A {@link Set} of this {@link User}'s friends, this {@link List} is NOT modifiable.
   */
  Set<User> getFollowing();
}
