package com.gwylim.applications.postit.web.server.basic;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.PostitService;
import com.gwylim.applications.postit.services.UserService;

/**
 * Spring MVC controller for the webapp.
 *
 * This class handles display of the front page.
 *
 * @author Gwylim
 *
 */
@Controller
public class RootController {

  private static final Logger LOG = LoggerFactory.getLogger(RootController.class);

  private final UserService _userService;

  private final PostitService _postitService;

  /***/
  @Autowired
  public RootController(final UserService userService, final PostitService postitService) {
    _postitService = postitService;
    LOG.debug("Created RootController.");
    _userService = userService;
  }

  /**
   * Indirection to allow selection if default front page.
   */
  @RequestMapping("/")
  public String frontRedirect() throws NoSuchUserException {
    return "redirect:/inlineBackbone";
  }

  /**
   * The main page of the application when a User is logged in it displays their postits.
   * When no user is logged in it redirects to the signin page.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping("/static")
  public String frontPage(final Model model, final Principal principal) throws NoSuchUserException {
    final String username = principal.getName();

    final User user = _userService.getUser(username);

    model.addAttribute("postits", user.getPostits());
    model.addAttribute("following", user.getFollowing());
    model.addAttribute("followingPostits", _postitService.getFollowedPostits(user));

    return "FrontPage";
  }


  /**
   * Serve the base page for the JQuery version of the dynamic front page.
   */
  @RequestMapping("/ajax")
  public String ajaxFrontPage() {
    LOG.info("Request for AXAX front page.");
    return "AJAX"; // Return the AJAX page template.
  }

  /**
   * Serve the base page for the Backbone version of the dynamic front page.
   */
  @RequestMapping("/backbone")
  public String backboneFrontPage() {
    LOG.info("Request for BACKBONE front page.");
    return "Backbone"; // Return the Backbone page template.
  }

  /**
   * Serve the base page for the Backbone version of the dynamic front page.
   */
  @RequestMapping("/inlineBackbone")
  public String inlineBackboneFrontPage() {
    LOG.info("Request for INLINE BACKBONE front page.");
    return "InlineBackbone"; // Return the InlineBackbone page template.
  }
}
