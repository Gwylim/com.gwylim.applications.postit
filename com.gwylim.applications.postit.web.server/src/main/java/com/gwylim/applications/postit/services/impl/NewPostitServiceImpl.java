package com.gwylim.applications.postit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gwylim.applications.postit.dao.postit.HibernatePostit;
import com.gwylim.applications.postit.dao.postit.PostitDAO;
import com.gwylim.applications.postit.dao.user.UserDAO;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NewPostitService;
import com.gwylim.applications.postit.services.NoSuchUserException;

/**
 * Implementation of the {@link NewPostitService}.
 *
 * @author Gwylim
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class NewPostitServiceImpl implements NewPostitService {
  private final UserDAO _userDAO;
  private final PostitDAO _postitDAO;

  /***/
  @Autowired
  public NewPostitServiceImpl(final UserDAO userDAO, final PostitDAO postitDAO) {
    _userDAO = userDAO;
    _postitDAO = postitDAO;
  }

  /**
   * @see NewPostitService#newPostit(String, String, User)
   */
  @Override
  public Postit newPostit(final String title, final String body, final User user) throws NoSuchUserException {
    return newPostit(title, body, user.getUserID());
  }

  /**
   * @see NewPostitService#newPostit(String, String, UserID)
   */
  @Override
  public Postit newPostit(final String title, final String body, final UserID authorId) throws NoSuchUserException {
    final User user = _userDAO.getUser(authorId);
    if (user == null) { throw new NoSuchUserException("User '" + authorId + "' does not exist."); }

    final Postit postit = new HibernatePostit(title, body, user); // TODO: HibernatePostit should not be referenced outside DAO impl packages.
    _postitDAO.save(postit);

    return postit;
  }
}
