package com.gwylim.applications.postit.web.server.REST;

import org.springframework.http.HttpStatus;

/**
 * Thrown when there is an execption that should be translated to an HTTP status code.
 */
@SuppressWarnings("serial")
public class HTTPException extends Exception {

  private final HttpStatus _status;

  /**
   * Create a new exception with the given code.
   * @param status  The status code.
   */
  public HTTPException(final HttpStatus status) {
    _status = status;
  }

  public int getStatus() { return _status.value(); }

}
