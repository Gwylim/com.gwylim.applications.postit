package com.gwylim.applications.postit.dao;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configuration for the hibernate subsystem.
 */
@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
public class HibernateConfiguration {
  /**
   * Create the session factory.
   * This was not needed before autowiring UserDetailsService into WebSecurityConfig.
   * @return A new {@link SessionFactory}.
   */
  @Bean
  public SessionFactory sessionFactory(final DataSource dataSource) {
    final LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource);
    builder.scanPackages("com.gwylim.applications.postit.dao");

    return builder.buildSessionFactory();
  }
}
