package com.gwylim.applications.postit.web.server.basic.formData;

/**
 * Provides a check method for data length and a method for checking if errors are present.
 */
public abstract class CheckedData {
  public static final int MAX_FIELD_LENGTH = 255;
  public static final String MAXIMUM_CHARACTERS_EXCEEDED_ERROR = "Maximum number of characters (255) exceeded.";
  public static final String NO_CONTENT_ENTERED_ERROR = "No content entered.";

  /**
   * Check the given field for error.
   * @param field The field string to check.
   * @return      The error message or <code>null</code> if no error.
   */
  protected String check(final String field) {
    if (field.length() > MAX_FIELD_LENGTH) {
      return MAXIMUM_CHARACTERS_EXCEEDED_ERROR;
    }
    else if (field.isEmpty()) {
      return NO_CONTENT_ENTERED_ERROR;
    }
    else {
      return null;
    }
  }

  /**
   * Does this data have errors.
   */
  public abstract boolean hasErrors();
}
