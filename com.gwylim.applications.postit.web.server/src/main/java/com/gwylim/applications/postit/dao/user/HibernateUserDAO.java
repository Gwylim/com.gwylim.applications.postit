package com.gwylim.applications.postit.dao.user;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gwylim.applications.postit.dao.HibernateDAO;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;

/**
 * @see UserDAO.
 */
@Component
public class HibernateUserDAO extends HibernateDAO implements UserDAO {

  /**
   * Create a {@link HibernateUserDAO} for testing.
   * @param sessionFactory The {@link SessionFactory} to use for testing.
   */
  @Autowired
  public HibernateUserDAO(final SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public UserID newUser(final String name, final String encodedPassword) {
    return new UserID((String) getSession().save(new HibernateUser(name, encodedPassword, this)));
  }

  @Override
  public void updateUsername(final User user, final String username) {
    final HibernateUser hibernateUser = (HibernateUser) user;
    hibernateUser.setUsername(username);
    getSession().merge(user);
  }

  @Override
  public void delete(final User user) {
    getSession().delete(user);
  }

  @Override
  public void delete(final UserID userID) {
    getSession().delete(getUser(userID));
  }

  @Override
  public HibernateUser getUser(final String username) throws NoSuchUserException {
    final HibernateUser user = (HibernateUser) getSession().createCriteria(HibernateUser.class).add(Restrictions.eq("username", username)).uniqueResult();
    if (user != null) {
      user.setHibernateUserDAO(this);
      return user;
    }
    else {
      throw new NoSuchUserException("No such username: " + username);
    }
  }

  @Override
  public HibernateUser getUser(final UserID id) {
    final HibernateUser user = (HibernateUser) getSession().get(HibernateUser.class, id.toDB());
    if (user != null) { user.setHibernateUserDAO(this); }
    return user;
  }

  @Override
  public void follow(final User follower, final User followed) {
    final HibernateUser hibernateFollower = getUser(follower.getUserID());
    final HibernateUser hibernateFollowed = getUser(followed.getUserID());
    hibernateFollower.getHibernateFollowing().add(hibernateFollowed);
    getSession().merge(hibernateFollower);
  }

  @Override
  public void unfollow(final User follower, final User followed) {
    final HibernateUser hibernateFollower = getUser(follower.getUserID());
    final HibernateUser hibernateFollowed = getUser(followed.getUserID());
    hibernateFollower.getHibernateFollowing().remove(hibernateFollowed);
    getSession().merge(hibernateFollower);
  }
}
