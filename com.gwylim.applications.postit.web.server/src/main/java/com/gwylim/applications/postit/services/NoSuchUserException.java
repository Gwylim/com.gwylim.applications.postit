package com.gwylim.applications.postit.services;

/**
 * Exception that occurs when an attempt is made to get a user by ID that does not exist.
 */
@SuppressWarnings("serial")
public class NoSuchUserException extends Exception {

  /***/
  public NoSuchUserException(final String message) {
    super(message);
  }
}

