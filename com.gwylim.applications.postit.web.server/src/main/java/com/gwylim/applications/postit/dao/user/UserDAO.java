package com.gwylim.applications.postit.dao.user;

import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;

/**
 * Data access object for performing operations on {@link User}s.
 */
public interface UserDAO {

  /**
   * Create a new {@link User} in the system.
   * @param name  The name of the {@link User} to create.
   * @param encodedPassword The encoded password to save to the database.
   * @return      The newly created {@link User}.
   */
  UserID newUser(String name, String encodedPassword);

  /**
   * Get a {@link User} by username from the database.
   * @param username  The username of the required {@link HibernateUser}.
   * @return The user that has the given user name.
   * @throws NoSuchUserException When the user could not be found.
   */
  User getUser(String username) throws NoSuchUserException;

  /**
   * Get a {@link User} from the database by ID.
   * @param id  The Id to look for in the database.
   * @return The user for the given ID.
   */
  User getUser(UserID id);

  /**
   * Delete the supplied {@link User} from the database.
   * @param user The user to delete.
   */
  void delete(User user);

  /**
   * Delete the supplied {@link User} from the database by ID.
   * @param userID The user to delete by ID.
   */
  void delete(UserID userID);

  /**
   * Update the username of the given {@link User}.
   * @param user        The {@link User} to update.
   * @param newUsername The new username for the {@link User}.
   */
  void updateUsername(User user, String newUsername);

  /**
   * Add one user to the following list of another.
   * @param follower new follower of followed user.
   * @param followed the user being followed.
   */
  void follow(final User follower, final User followed);

  /**
   * Remove followed from the set of users followed by follower.
   */
  void unfollow(User follower, User followed);
}
