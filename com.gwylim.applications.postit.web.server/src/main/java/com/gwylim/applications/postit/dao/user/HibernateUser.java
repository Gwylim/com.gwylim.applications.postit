package com.gwylim.applications.postit.dao.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import com.gwylim.applications.postit.dao.HibernateDAO;
import com.gwylim.applications.postit.dao.postit.HibernatePostit;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;

/**
 * A user stored in Hibernate ORM.
 *
 * @author Gwylim
 */
@Entity
@Table(name = "users")
public class HibernateUser implements User {

  private UserID _id = new UserID();

  private String _username;
  private String _password;

  private List<HibernatePostit> _postits;
  private Set<HibernateUser> _friends;

  private HibernateDAO _hibernateDAO;
  void setHibernateUserDAO(final HibernateUserDAO hibernateDAO) { _hibernateDAO = hibernateDAO; }

  /**
   * For deserialisation.
   */
  @SuppressWarnings("unused")
  private HibernateUser() { }

  /**
   * Create a new User.
   * @param username        The name of the new user.
   * @param encodedPassword The encoded password for the user.
   * @param hibernateDAO    A HibernateDAO for lazy initialisation.
   */
  public HibernateUser(final String username, final String encodedPassword, final HibernateDAO hibernateDAO) {
    _password = encodedPassword;
    _username = username;
    _hibernateDAO = hibernateDAO;
    setHibernatePostits(new ArrayList<>());
    setHibernateFollowing(new LinkedHashSet<>());
  }

  @Id
  @Column(name = "id")
  private String getId() { return _id.toDB(); }
  @SuppressWarnings("unused")
  private void setId(final String id) { _id = new UserID(id.toString()); }

  /**
   * @see com.gwylim.applications.postit.web.server.model.User#getUserID()
   */
  @Override
  @Transient
  public UserID getUserID() { return _id; }


  /**
   * @see com.gwylim.applications.postit.web.server.model.User#getUsername()
   */
  @Override
  @Column(name = "username", nullable = false, unique = true)
  public String getUsername() { return _username; }
  public void setUsername(final String username) { _username = username; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.User#getPassword()
   */
  @Override
  @Column(name = "password", nullable = false)
  public String getPassword() { return _password; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.User#setPassword(java.lang.String)
   */
  @Override
  public void setPassword(final String password) { _password = password; }


  /**
   * @see com.gwylim.applications.postit.web.server.model.User#getPostits()
   */
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "hibernateAuthor", orphanRemoval = true, fetch = FetchType.LAZY)
  List<HibernatePostit> getHibernatePostits() { return _postits; }
  private void setHibernatePostits(final List<HibernatePostit> postits) { _postits = postits; }

  @Override
  @Transient
  public List<Postit> getPostits() {
    _hibernateDAO.run(session -> {
      session.update(HibernateUser.this);
      Hibernate.initialize(HibernateUser.this.getHibernatePostits()); // Has to be getHibernatePostits rather than getPostits so the lazy loading proxy is hit.
    });
    return Collections.unmodifiableList(_postits);
  }


  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(
      name = "Follows",
      joinColumns = @JoinColumn(name = "Follower_id"),
      inverseJoinColumns = @JoinColumn(name = "Followed_id")
  )
  Set<HibernateUser> getHibernateFollowing() { return _friends; }
  private void setHibernateFollowing(final Set<HibernateUser> friends) { _friends = friends; }

  /**
   * @see com.gwylim.applications.postit.web.server.model.User#getFollowing()
   */
  @Override
  @Transient
  public Set<User> getFollowing() {
    _hibernateDAO.run(session -> {
      session.update(HibernateUser.this);
      Hibernate.initialize(HibernateUser.this.getHibernateFollowing()); // Has to be getHibernateFriends rather than getFriends so the lazy loading proxy is hit.
    });
    return Collections.unmodifiableSet(_friends);
  }

  @Override
  public String toString() {
    return "User '" + _username + "'";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (_id == null ? 0 : _id.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) { return true; }
    if (obj == null) { return false; }
    if (getClass() != obj.getClass()) { return false; }
    final HibernateUser other = (HibernateUser) obj;
    if (_id == null) {
      if (other._id != null) {
        return false;
      }
    }
    else if (!_id.equals(other._id)) {
      return false;
    }
    return true;
  }
}
