package com.gwylim.applications.postit.services;

import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;

/**
 * Creates postits.
 */
public interface NewPostitService {

  /**
   * Create a new Postit with the supplied content.
   * @param title   The title of the postit.
   * @param body    The body of the postit.
   * @param userID  The author User of the postit.
   * @return        The newly created postit.
   * @throws NoSuchUserException  When the indicated user does not exist in the database.
   */
  Postit newPostit(String title, String body, UserID userID) throws NoSuchUserException;

  /**
   * Create a new Postit with the supplied content.
   * @param title   The title of the postit.
   * @param body    The body of the postit.
   * @param user    The author User of the postit.
   * @return        The newly created postit.
   * @throws NoSuchUserException  When the indicated user does not exist in the database.
   */
  Postit newPostit(String title, String body, User user) throws NoSuchUserException;
}
