package com.gwylim.applications.postit.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base class for all hibernate DAOs.
 * Supplies {@link Session}s.
 */
public abstract class HibernateDAO {

  private final SessionFactory _sessionFactory;

  /***/
  @Autowired
  public HibernateDAO(final SessionFactory sessionFactory) {
    _sessionFactory = sessionFactory;
  }


  /**
   * Get a session to work with.
   */
  public Session getSession() {
    return _sessionFactory.getCurrentSession();
  }

  /**
   * Create a new session.
   */
  private Session createSession() {
    return _sessionFactory.openSession();
  }

  /**
   * @see HibernateDAO#run(SessionedAction)
   */
  public void run(final SessionedAction sessionedAction) {
    Session session = null;
    boolean got;
    try {
      session = getSession();
      got = true;
    }
    catch (final HibernateException e) { // No current session; create one.
      got = false;
    }

    if (session == null) {
      session = createSession();
      got = false;
    }

    try {
      sessionedAction.run(session);
    }
    finally {
      if (!got) { // If we opened a session we should close it.
        session.close();
      }
    }
  }
}
