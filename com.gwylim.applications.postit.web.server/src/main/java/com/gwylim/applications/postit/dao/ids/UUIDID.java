package com.gwylim.applications.postit.dao.ids;

import java.util.UUID;

/**
 * Random UUID implementation of ID.
 *
 * @author Gwylim
 */
public class UUIDID extends ID<String> {
  private final UUID _id;

  public UUIDID() { _id = UUID.randomUUID(); }
  public UUIDID(final String uuid) { _id = UUID.fromString(uuid); }

  @Override
  public final String toDB() { return _id.toString(); }
  @Override
  public String toString() { return toDB().toString(); }
}