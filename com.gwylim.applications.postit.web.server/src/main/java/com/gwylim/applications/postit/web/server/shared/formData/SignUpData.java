package com.gwylim.applications.postit.web.server.shared.formData;

import com.gwylim.applications.postit.web.server.basic.formData.CheckedData;

/**
 * Object passed back from a Sign In form, see SignIn.html.
 */
public class SignUpData extends CheckedData {

  /** The supplied username. */
  private String _username;

  /** The supplied password. */
  private String _password;

  private String _usernameError;
  private String _passwordError;

  public String getUsername() { return _username; }
  /***/
  public void setUsername(final String username) {
    _username = username;
    _usernameError = check(_username);
  }

  public String getPassword() { return _password; }
  /***/
  public void setPassword(final String password) {
    _password = password;
    _passwordError = check(_password);
  }

  @Override
  public boolean hasErrors() { return _usernameError != null || _passwordError != null; }

  public String getUsernameError() { return _usernameError; }
  public void setUsernameError(final String usernameError) { _usernameError = usernameError; }

  public String getPasswordError() { return _passwordError; }
  public void setPasswordError(final String passwordError) { _passwordError = passwordError; }

  @Override
  public String toString() {
    return "SignUpData [Username=" + _username + ", Password=" + _password + "]";
  }
}
