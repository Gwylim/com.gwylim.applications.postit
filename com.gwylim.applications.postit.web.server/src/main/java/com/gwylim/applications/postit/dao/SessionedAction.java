package com.gwylim.applications.postit.dao;

import org.hibernate.Session;

/**
 * An action that is supplied with a session to run in/against.
 * NOTE: session is NOT always transaction bound so make sure to wrap such actions in a transaction externally.
 * The session used will be the existing transactional session if one exists.
 * If none exist it will be a non transactional session that will be created.
 */
@FunctionalInterface
public interface SessionedAction {
  /**
   * Provide the code to run against the provided session.
   */
  void run(Session session);
}
