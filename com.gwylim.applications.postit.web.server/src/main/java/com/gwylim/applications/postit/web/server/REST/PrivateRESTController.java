package com.gwylim.applications.postit.web.server.REST;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.NewPostitService;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.PostitService;
import com.gwylim.applications.postit.services.UserService;
import com.gwylim.applications.postit.web.server.REST.model.RESTPostit;

/**
 * Controller for serving the private/signed in REST api.
 *
 * @author Gwylim
 */
@RestController
@RequestMapping("/api/private/postits")
public class PrivateRESTController {

  private static final Logger LOG = LoggerFactory.getLogger(PrivateRESTController.class);

  private final UserService _userService;
  private final PostitService _postitService;
  private final NewPostitService _newPostitService;

  /***/
  @Autowired
  public PrivateRESTController(final UserService userService, final PostitService postitService, final NewPostitService newPostitService) {
    _userService = userService;
    _postitService = postitService;
    _newPostitService = newPostitService;
  }


  /**
   * Create a new the postit for the currently logged in user.
   * @param principal The security principal from which we can get the user.
   * @return The created postit.
   * @throws NoSuchUserException When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(method = RequestMethod.POST)
  public RESTPostit create(final Principal principal, @RequestBody final RESTPostit restPostit) throws NoSuchUserException {
    final String username = principal.getName();

    final User user = _userService.getUser(username);
    final Postit postit = _newPostitService.newPostit(restPostit.getTitle(), restPostit.getBody(), user);
    LOG.info(username + " request to create postit: " + postit);

    return new RESTPostit(postit);
  }


  /**
   * Get the postits for the currently logged in user.
   * @param principal The security principal from which we can get the user.
   * @return  A {@link List} of this user's postits.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(method = RequestMethod.GET)
  public List<RESTPostit> fetch(final Principal principal) throws NoSuchUserException {
    final String username = principal.getName();
    final User user = _userService.getUser(username);
    final List<Postit> postits = user.getPostits();

    LOG.info(username + " requesting postits; returning " + postits.size());

    return postits.stream()
        .map(t -> new RESTPostit(t))
        .collect(Collectors.toList());
  }


  /**
   * Get a single postit by id for the currently logged in user.
   * @param principal The security principal from which we can get the user.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(method = RequestMethod.GET, value = "{id}")
  public RESTPostit fetchOne(final Principal principal, @PathVariable("id") final String id) throws HTTPException, NoSuchUserException {
    final String username = principal.getName();
    final User user = _userService.getUser(username);

    final Postit postit = _postitService.getPostit(new PostitID(id));

    if (postit != null) {
      if (postit.getAuthor().equals(user) || postit.getAuthor().getFollowing().contains(postit.getAuthor())) {
        LOG.info(username + " requested postit; returning " + postit);
        return new RESTPostit(postit);
      }
      else {
        throw new HTTPException(HttpStatus.FORBIDDEN); // The current user does not own the postit and is not following the owner.
      }
    }
    else {
      throw new HTTPException(HttpStatus.NOT_FOUND); // The postit could not be found.
    }
  }


  /**
   * Edit an existing postit.
   * @param principal The security principal from which we can get the user.
   * @return The updated postit.
   * @throws NoSuchUserException When the user does not exist... but given we have authenticated there has been a server error.
   * @throws HTTPException When there is an issue updating the postit.
   */
  @RequestMapping(method = RequestMethod.PUT, value = "{id}")
  public RESTPostit save(final Principal principal, @RequestBody final RESTPostit restPostit, @PathVariable("id") final String id) throws NoSuchUserException, HTTPException {
    final String username = principal.getName();

    final User user = _userService.getUser(username);

    final Postit postit = _postitService.getPostit(new PostitID(id));
    if (postit == null) {
      LOG.warn("Attempt to update postit " + id + " that does not exist.");
      throw new HTTPException(HttpStatus.NOT_FOUND);
    }

    if (!postit.getAuthor().equals(user)) {
      LOG.warn(user + " attempted to update a postit that was not theirs: " + id);
      throw new HTTPException(HttpStatus.FORBIDDEN); // User does not own the postit they are attempting to edit.
    }

    LOG.info(username + " request to edit existing postit: " + postit);

    final Postit updatedPostit = _postitService.update(postit, restPostit.getTitle(), restPostit.getBody());
    return new RESTPostit(updatedPostit);
  }


  /**
   * Request the deletion of a postit by ID.
   * @param principal The security principal from which we can get the user.
   * @param id The id of the {@link Postit} to delete.
   * @throws NoSuchUserException  When the user does not exist... but given we have authenticated there has been a server error.
   */
  @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
  public RESTPostit deletePostit(final Principal principal, @PathVariable("id") final String id) throws NoSuchUserException {
    final String username = principal.getName();
    final User user = _userService.getUser(username);

    final Postit postit = _postitService.getPostit(new PostitID(id));
    if (postit == null) {
      LOG.warn("Attempt to destroy non-existant postit.");
    }
    else {
      if (postit.getAuthor().equals(user)) {
        LOG.info("Author " + username + " requesting delete of postit " + postit);
        _postitService.delete(postit);
      }
      else {
        LOG.warn(username + " requesting delete of postit " + id + " that is not theirs!");
      }
    }
    return new RESTPostit(postit);
  }



  /**
   * Translate {@link HTTPException}s to responses codes.
   */
  @ExceptionHandler(HTTPException.class)
  public String handleAppException(final HTTPException e, final HttpServletResponse response) {
    response.setStatus(e.getStatus());
    return e.getMessage();
  }

  /**
   * Translate other exceptions to internal error.
   * @param e The thrown exception.
   * @return  The response body text.
   */
  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public String handleAppException(final Exception e) {
    LOG.warn("Returning 500 to client, caused by: ", e);
    return e.getMessage();
  }
}
