package com.gwylim.applications.postit.model;

import com.gwylim.applications.postit.dao.ids.UUIDID;

/**
 * Typed extension of ID for User Objects.
 *
 * @author Gwylim
 */
public class UserID extends UUIDID {
  public UserID() { super(); }
  public UserID(final String id) { super(id); }
}
