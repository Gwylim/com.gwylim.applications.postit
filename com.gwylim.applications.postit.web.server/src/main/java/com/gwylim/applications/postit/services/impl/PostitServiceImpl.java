package com.gwylim.applications.postit.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gwylim.applications.postit.dao.postit.PostitDAO;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.PostitService;

/**
 * Data access object for performing operations on HibernatePostits.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PostitServiceImpl implements PostitService {

  private final PostitDAO _postitDAO;

  /***/
  @Autowired
  public PostitServiceImpl(final PostitDAO postitDAO) {
    _postitDAO = postitDAO;
  }

  /**
   * Get a postit by id.
   * @param id  The id of the Postit to get.
   * @return    The postit or <code>null</code> if no postit exists with that ID.
   */
  @Override
  public Postit getPostit(final PostitID id) {
    return _postitDAO.getPostit(id);
  }

  @Override
  public Postit update(final Postit postit, final String title, final String body) {
    return _postitDAO.update(postit, title, body);
  }

  @Override
  public void delete(final Postit postit) {
    _postitDAO.delete(postit);
  }

  @Override
  public List<Postit> getFollowedPostits(final User user) {
    return _postitDAO.getFollowedPostits(user);
  }
}
