// Backbone implementation of the postit page.

$(function(){

  // Template function for generating the list of postits.
  dust.loadSource(dust.compile($("#postitTemplate").html(), "postitTemplate"));
  dust.loadSource(dust.compile($("#postitListTemplate").html(), "postitsTemplate"));

  // Postit collection, used to load all the postits.
  var PostitCollection = Backbone.Collection.extend({ url: "/api/private/postits" });
  var postitCollection = new PostitCollection();
  
  // Postit Model, used to work with individual postits.
  var PostitModel = Backbone.Model.extend({ urlRoot: "/api/private/postits" });
  var postitModel = new PostitModel();


  // View that lists all the postits.
  var PostitListView = Backbone.View.extend({
    el: "#content",
    render: function () {
      var targetElement = this.$el;
      
      // Load and render the list of postits.
      postitCollection.fetch({
        success: function(postits) {
          console.log("Rendering postits: " + JSON.stringify(postits.models));

          var data = {"postits": postits.toJSON()};

          dust.render("postitsTemplate", data, function(err, out) {
            targetElement.html(out);
          });
        },
        error: function() {
          alert("Failed to load postit collection.");
        }
      });
    },
    events : {
      "click .edit"      : "edit",
      "click .cancel"    : "cancel",
      "click .create"    : "save",
      
      "click .delete"    : "del",
      "click #newPostit" : "create"
    },
    edit : function(ev) {
      var postitEl = $(ev.target).closest(".postit");
      var postitID = postitEl.attr('name');
      console.log("edit mode: " + postitID);
      
      // Load postit data for editing.
      var postit = new PostitModel({id: postitID});
      postit.fetch({
        success : function (postit) {
          console.log("Loaded data" + JSON.stringify(postit));
          
          postitEl.find("input.postitTitle").val(postit.get("title"));
          postitEl.find("textarea.postitBody").val(postit.get("body"));
          postitEl.find("div.postitTitle").text(postit.get("title"));
          postitEl.find("div.postitBody").text(postit.get("body"));
          
          postitEl.addClass("editing");
        }
      });
    },
    cancel : function(ev) {
      var postit = $(ev.target).closest(".postit");
      var postitID = postit.attr('name');
      
      if (postitID) {
        console.log("cancel edit mode: " + postit.attr('name'));
        postit.removeClass("editing");
      }  
      else {
        console.log("cancel postit creation.");
        postit.remove();
      }
    },
    save : function (ev) {
      var postitEl = $(ev.target).closest(".postit");
      
      var newPostit = {
        id    : postitEl.attr('name') ? postitEl.attr('name') : null,
        title : postitEl.find("input.postitTitle").val(),
        body  : postitEl.find("textarea.postitBody").val()
      };
      console.log("Saving postit " + JSON.stringify(newPostit));
      
      postitModel.save(newPostit, {
        success: function (newPostit) {
          console.log("Saved postit " + JSON.stringify(newPostit));
          
          postitEl.attr('name', newPostit.get("id"))
          postitEl.find("input.postitTitle").val(newPostit.get("title"));
          postitEl.find("textarea.postitBody").val(newPostit.get("body"));
          postitEl.find("div.postitTitle").text(newPostit.get("title"));
          postitEl.find("div.postitBody").text(newPostit.get("body"));
          
          postitEl.removeClass("editing");
        }
      });
    },
    del : function(ev) {
      var postitID = $(ev.target).closest(".postit").attr('name');
      console.log("delete: " + postitID);
      
      var toDelete = new PostitModel({id : postitID});
      toDelete.destroy({
        success: function () {
          console.log("Deleted.");
          postitListView.render(); // Re-render the postit list... using postitListView reference is a bit unpleasant...
        },
        error: function () {
          console.log("Error deleting.");
          postitListView.render(); // Re-render the list as this error may have been cause by it already having been deleted.
        }
      });
      
      ev.stopPropagation(); // Do not propagate the event from delete to edit.
    },
    
    create: function(ev) {
      // Add a new previous sibling
      var addEl = $(ev.target).closest(".postit");
      
      dust.render("postitTemplate", {}, function(err, out) {
        $(out).insertBefore(addEl);
      });
    }
  });



  var Router = Backbone.Router.extend({
    routes: {
      '' : 'home'
    }
  });
  var router = new Router();

  var postitListView = new PostitListView();
  router.on("route:home",       function()   { postitListView.render(); });

  Backbone.history.start();
});