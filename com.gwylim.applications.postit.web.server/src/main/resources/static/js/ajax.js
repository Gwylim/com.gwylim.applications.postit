$(function(){
  var postitsList = $("#postits");

  // Load the postit list for the first time.
  loadPostits();
  
  // Add button handler to do a POST of the postit data.
  $("#add").on("click", function() {
    var newPostit = {
      title: $("#title").val(),
      body: $("#body").val()
    }
    resetForm();
    
    console.log("POST: " + JSON.stringify(newPostit));
    $.ajax({
      type: "POST",
      url: "/api/private/postits",
      data: JSON.stringify(newPostit),
      contentType: "application/json",
      success: function(postit) {
        loadPostits(); // Could probably just insert postit instead...
      }
    });
  })
  
  // Loads the posits via AJAX and populates the list.
  function loadPostits() {
    $.ajax({
      type: "GET",
      url: "/api/private/postits",
      beforeSend: function() {
        postitsList.html("<li>Waiting for postits...</li>");
      },
      success: function(postits) {
        postitsList.empty();
        $.each(postits, function(i, postit) {
          var li = $("<li class='postitListItem' name='" + postit.id + "'></li>").text(postit.title + " -- " + postit.body);
          postitsList.append(li);
        });
      }
    });
  }
  
  // Attache delete listeners to the list of postits.
  // Listen to the whole list and delegate (fire on click of and scope to) to the .delete class elements.
  
  // DELETE BUTTON ACTION:
  $("#postits").delegate(".delete", "click", function(e) {
    var postitElement=$(this).closest("li");
    var postitID=postitElement.attr("name");
    
    e.stopPropagation(); // Do not propagate the event to the list item as this will cause an update selection.
    
    console.log("DELETE: " + postitID );
    $.ajax({
      type: "DELETE",
      url: "/api/private/postits/" + postitID,
      beforeSend: function() {
        postitElement.html("Deleting...");
      },
      success: function(postits) {
        loadPostits();
      }
    });
  });
  
  
  // CLICKING ON A LIST ITEM POPULATES THE FORM AND PUTS IT IN UPDATE MODE.
  var editingID = undefined;

  $("#postits").delegate("li.postitListItem", "click", function() {
    var postitElement=$(this)
    editingID=postitElement.attr("name");
    
    console.log("Edit mode set for: " + editingID );
    
    $.ajax({
      type: "GET",
      url: "/api/private/postits/" + editingID,
      success: function(postit) {
        if (editingID == postit.id) { // Incase another click has happened since this was fired.
          $("#title").val(postit.title);
          $("#body").val(postit.body);
          
          $("#add").hide();
          $("#update").show();
        }
      }
    });
  });
  
  
  // Add button handler to do a POST of the postit data.
  $("#update").on("click", function() {
    var newPostit = {
      id: editingID,
      title: $("#title").val(),
      body: $("#body").val()
    }
    resetForm();
    
    console.log("PUT: " + JSON.stringify(newPostit));
    $.ajax({
      type: "PUT",
      url: "/api/private/postits/" + editingID,
      data: JSON.stringify(newPostit),
      contentType: "application/json",
      success: function(postit) {
        loadPostits(); // Could probably just insert postit instead...
      }
    });
  });
  
  
  // CLEAR BUTTON; Empty form and reset buttons to add mode.
  $("#clear").on("click", function () {
    resetForm();
  });
  
  function resetForm() {
    $("#add").show();
    $("#update").hide();
    
    $("#title").val("");
    $("#body").val("");
  }
});