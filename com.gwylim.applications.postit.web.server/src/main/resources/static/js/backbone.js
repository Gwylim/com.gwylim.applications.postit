// Backbone implementation of the postit page.

$(function(){
  // Template function for generating the list of postits.
  dust.loadSource(dust.compile($("#postitListTemplate").html(), "postits"));
  dust.loadSource(dust.compile($("#editPostitTemplate").html(), "editPostit"));

  // Postit collection, used to load all the postits.
  var PostitCollection = Backbone.Collection.extend({ url: "/api/private/postits" });
  var postitCollection = new PostitCollection();
  
  // Postit Model, used to work with individual postits.
  var PostitModel = Backbone.Model.extend({ urlRoot: "/api/private/postits" });
  var postitModel = new PostitModel();


  // View that lists all the postits.
  var PostitListView = Backbone.View.extend({
    el: "#content",
    render: function () {
      var targetElement = this.$el;
      
      // Load and render the list of postits.
      postitCollection.fetch({
        success: function(postits) {
          console.log("Rendering postits: " + JSON.stringify(postits.models));

          var data = {"postits": postits.toJSON()};

          dust.render("postits", data, function(err, out) {
            targetElement.html(out);
          });
        },
        error: function() {
          alert("Failed to load postit collection.");
        }
      });
    },
    events : {
      "click .edit"   : "edit",
      "click .delete" : "del"
    },
    edit : function(ev) {
      var userId = $(ev.target).closest(".postit").attr('name');
      console.log("edit: " + userId);
      router.navigate("edit/" + userId, {trigger : true});
    },
    del : function(ev) {
      var userId = $(ev.target).closest(".postit").attr('name');
      console.log("delete: " + userId);
      
      var toDelete = new PostitModel({id : userId});
      toDelete.destroy({
        success: function () {
          console.log("Deleted.");
          postitListView.render(); // Re-render the postit list... using postitListView reference is a bit unpleasant...
        },
        error: function () {
          console.log("Error deleting.");
          postitListView.render(); // Re-render the list as this error may have been cause by it already having been deleted.
        }
      });
      
      ev.stopPropagation(); // Do not propagate the event from delete to edit.
    },
  });
  
  // View that handles postit creation.
  var EditPostitView = Backbone.View.extend({
    el: "#content",
    render: function (data) {
      console.log("EditPostitView.render");
      var targetElement = this.$el;
      
      if (data.id) { // If the ID has been set, load the postit data to edit.
        var postit = new PostitModel({id: data.id});
        postit.fetch({
          success : function (postit) {
            console.log("Render edit for: " + postit.toJSON())
            dust.render("editPostit", postit.toJSON(), function(err, out) {
              targetElement.html(out);
            });
          }
        });
      }
      else { // Otherwise create a blank edit form.
        dust.render("editPostit", {}, function(err, out) {
          targetElement.html(out);
        });
      }
    },
    events : {
      "click #save" :    "save",
      "click #cancel" : "cancel"
    },
    save : function (ev) {
      var newPostit = {
        id    : $("#id").val() == "" ? null : $("#id").val(),
        title : $("#title").val(),
        body  : $("#body").val()
      };
      
      postitModel.save(newPostit, {
        success: function () {
          console.log("Saved postit " + JSON.stringify(newPostit));
          router.navigate("", {trigger : true});
        }
      });
    },
    cancel : function (ev) {
      router.navigate("", {trigger : true});
    }
  });


  var Router = Backbone.Router.extend({
    routes: {
      ''          : 'home',
      'new'       : 'editPostit',
      'edit/:id'  : 'editPostit'
    }
  });
  var router = new Router();


  var postitListView = new PostitListView();
  var editPostitView = new EditPostitView();
  
  router.on("route:home",       function()   { postitListView.render(); });
  router.on("route:editPostit", function(id) { editPostitView.render({id: id}); });

  Backbone.history.start();
});