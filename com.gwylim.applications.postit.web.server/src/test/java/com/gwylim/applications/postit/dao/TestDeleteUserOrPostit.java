package com.gwylim.applications.postit.dao;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test adding and deleting postits.
 */
public class TestDeleteUserOrPostit extends HibernareDAOTestBase {

  @Test
  public void testDeleteUser() throws UserCreationException, NoSuchUserException {
    final UserID user = createUser().getUserID();

    final Postit postit1 = getNewPostitService().newPostit("title text", "body text", user);
    final Postit postit2 = getNewPostitService().newPostit("title text", "body text", user);

    Assert.assertNotNull(postit1.toString(), postit1.getPostitID());
    Assert.assertNotNull(postit2.toString(), postit2.getPostitID());

    getUserService().delete(user);

    Assert.assertNull(getPostitService().getPostit(postit1.getPostitID()));
    Assert.assertNull(getPostitService().getPostit(postit2.getPostitID()));
  }

  @Test
  public void testDeletePostit() throws UserCreationException, NoSuchUserException {
    final UserID userID = createUser().getUserID();

    final Postit postit1 = getNewPostitService().newPostit("title text", "body text", userID);
    final Postit postit2 = getNewPostitService().newPostit("title text", "body text", userID);

    Assert.assertNotNull("User does not exist!", getUserService().getUser(userID));

    Assert.assertNotNull("Added postit has no ID: " + postit1.toString(), postit1.getPostitID());
    Assert.assertNotNull("Added postit has no ID: " + postit2.toString(), postit2.getPostitID());

    Assert.assertNotNull("Postit 1 not saved.", getPostitService().getPostit(postit1.getPostitID()));
    Assert.assertNotNull("Postit 2 not saved.", getPostitService().getPostit(postit2.getPostitID()));

    getPostitService().delete(getPostitService().getPostit(postit1.getPostitID()));

    // Check postits wrt user.
    Assert.assertNotNull("User has been unexpectedly deleted.", getUserService().getUser(userID));
    Assert.assertEquals(1, getUserService().getUser(userID).getPostits().size());
    Assert.assertTrue(getUserService().getUser(userID).getPostits().contains(postit2));

    // Check postits direct get.
    Assert.assertNull("Postit 1 has not been delete as expected.",    getPostitService().getPostit(postit1.getPostitID()));
    Assert.assertNotNull("Postit 2 unexpectedly deleted.",            getPostitService().getPostit(postit2.getPostitID()));
  }
}
