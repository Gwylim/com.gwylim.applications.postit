package com.gwylim.applications.postit.dao.user.following;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test the follow functionality at a DAO level.
 */
public class TestFollow extends HibernareDAOTestBase {

  @Test
  public void testAddFollow() throws UserCreationException, NoSuchUserException {
    final User user1 = createUser();
    final User user2 = createUser();

    getUserService().makeFriends(user1, user2);

    Assert.assertEquals(1, user1.getFollowing().size());
    Assert.assertEquals(user2, user1.getFollowing().iterator().next());

    Assert.assertEquals(1, user2.getFollowing().size());
    Assert.assertEquals(user1, user2.getFollowing().iterator().next());

    Assert.assertEquals(1, getUserService().getUser(user1.getUserID()).getFollowing().size());
    Assert.assertEquals(user2, getUserService().getUser(user1.getUserID()).getFollowing().iterator().next());

    Assert.assertEquals(1, getUserService().getUser(user2.getUserID()).getFollowing().size());
    Assert.assertEquals(user1, getUserService().getUser(user2.getUserID()).getFollowing().iterator().next());
  }

  @Test
  public void testRemoveFollow() throws UserCreationException, NoSuchUserException {
    final User user1 = createUser();
    final User user2 = createUser();

    getUserService().makeFriends(user1, user2);

    final Set<User> friends = getUserService().getUser(user1.getUserID()).getFollowing();
    Assert.assertEquals(1, friends.size());
    Assert.assertEquals(user2, friends.iterator().next());

    getUserService().unfollow(user1, user2);

    Assert.assertEquals(0, getUserService().getUser(user1.getUserID()).getFollowing().size());
    Assert.assertEquals(1, getUserService().getUser(user2.getUserID()).getFollowing().size());

    getUserService().unfollow(user2, user1);

    Assert.assertEquals(0, getUserService().getUser(user1.getUserID()).getFollowing().size());
    Assert.assertEquals(0, getUserService().getUser(user2.getUserID()).getFollowing().size());
  }

  @Test
  public void testDoubleFollow() throws Exception {
    final User user1 = createUser();
    final User user2 = createUser();

    getUserService().follow(user1, user2);

    Assert.assertEquals(1, getUserService().getUser(user1.getUserID()).getFollowing().size());
    Assert.assertEquals(user2, getUserService().getUser(user1.getUserID()).getFollowing().iterator().next());

    getUserService().follow(user1, user2);

    Assert.assertEquals(1, getUserService().getUser(user1.getUserID()).getFollowing().size());
    Assert.assertEquals(user2, getUserService().getUser(user1.getUserID()).getFollowing().iterator().next());

    getUserService().unfollow(user1, user2);

    Assert.assertEquals(0, getUserService().getUser(user1.getUserID()).getFollowing().size());
  }
}
