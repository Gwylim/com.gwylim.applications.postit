package com.gwylim.applications.postit.dao.postit;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test update and delete of postits at DAO level.
 */
public class TestHibernarePostitDAOUpdateDelete extends HibernareDAOTestBase {

  @Test
  public void testChangePostitContent() throws UserCreationException, NoSuchUserException {
    final UserID userID = createUser().getUserID();
    final PostitID postitID = getNewPostitService().newPostit("title text", "body text", userID).getPostitID();

    Assert.assertNotNull("Added postit has no ID.", postitID);

    final User user = getUserService().getUser(userID);
    final Postit postit = getPostitService().getPostit(postitID);

    Assert.assertTrue("User does not contain expected postit:" + postit, user.getPostits().contains(postit));
    Assert.assertEquals("title text", postit.getTitle());
    Assert.assertEquals("body text", postit.getBody());
    Assert.assertNotNull(postit.getPostitID());

    final String newTitle = "New Title";
    final String newBody = "New body";
    getPostitService().update(postit, newTitle, newBody);

    final Postit postit2 = getPostitService().getPostit(postit.getPostitID());

    Assert.assertEquals(user, user);
    Assert.assertEquals(newTitle, postit2.getTitle());
    Assert.assertEquals(newBody, postit2.getBody());

    Assert.assertEquals(postit, postit2);
  }

  @Test
  public void testDelete() throws Exception {
    final UserID userID = createUser().getUserID();

    final PostitID postitID = getNewPostitService().newPostit("title text", "body text", userID).getPostitID();
    Assert.assertNotNull("Added postit has no ID.", postitID);

    final Postit postit = getPostitService().getPostit(postitID);

    getPostitService().delete(postit);

    Assert.assertNull(getPostitService().getPostit(postit.getPostitID()));
    Assert.assertFalse(getUserService().getUser(userID).getPostits().contains(postit));
  }
}
