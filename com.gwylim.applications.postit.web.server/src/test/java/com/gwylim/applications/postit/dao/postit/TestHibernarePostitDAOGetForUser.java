package com.gwylim.applications.postit.dao.postit;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.PostitID;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test we can get the postits for users.
 */
public class TestHibernarePostitDAOGetForUser extends HibernareDAOTestBase {

  @Test
  public void testGetForUser() throws UserCreationException, NoSuchUserException {
    final UserID user = createUser().getUserID();

    final PostitID postitID = getNewPostitService().newPostit("title text", "body text", user).getPostitID();
    Assert.assertNotNull("Added postit has no ID.", postitID);

    final User gotUser = getUserService().getUser(user);
    final List<Postit> postitsForUser = gotUser.getPostits();
    Assert.assertEquals(1, postitsForUser.size());

    Assert.assertEquals(getPostitService().getPostit(postitID), postitsForUser.get(0));
  }

  @Test
  public void testGetForUsername() throws UserCreationException, NoSuchUserException {
    final UserID user = createUser().getUserID();

    final PostitID postitID = getNewPostitService().newPostit("title text", "body text", user).getPostitID();
    Assert.assertNotNull("Added postit has no ID.", postitID);

    final Postit postit = getPostitService().getPostit(postitID);

    final User gotUser = getUserService().getUser(user);
    final List<Postit> postitsForUser = gotUser.getPostits();
    Assert.assertEquals(1, postitsForUser.size());

    Assert.assertEquals(postit, postitsForUser.get(0));
  }

  @Test
  public void testGetMultipleForUsername() throws UserCreationException, NoSuchUserException {
    final UserID user = createUser().getUserID();

    final PostitID postitID1 = getNewPostitService().newPostit("1 title text", "body text", user).getPostitID();
    final PostitID postitID2 = getNewPostitService().newPostit("2 title text", "body text", user).getPostitID();

    Assert.assertNotNull("Added postit has no ID.", postitID1);
    Assert.assertNotNull("Added postit has no ID.", postitID2);

    final User gotUser = getUserService().getUser(user);
    final List<Postit> postitsForUser = gotUser.getPostits();
    Assert.assertEquals("Wrong number of postits... " + postitsForUser, 2, postitsForUser.size());

    // This is potentially dangerous, TODO: Add time stamp and order by.
    Assert.assertEquals(getPostitService().getPostit(postitID1), postitsForUser.get(0));
    Assert.assertEquals(getPostitService().getPostit(postitID2), postitsForUser.get(1));
  }

  @Test
  public void testGetMultipleForUser() throws UserCreationException, NoSuchUserException {
    final UserID user1 = createUser().getUserID();
    final UserID user2 = createUser().getUserID();

    final PostitID postitID1 = getNewPostitService().newPostit("title text", "body text", user1).getPostitID();
    final PostitID postitID2 = getNewPostitService().newPostit("title text", "body text", user2).getPostitID();

    Assert.assertNotNull("Added postit has no ID.", postitID1);
    Assert.assertNotNull("Added postit has no ID.", postitID2);


    final List<Postit> postitsForUser1 = getUserService().getUser(user1).getPostits();
    final List<Postit> postitsForUser2 = getUserService().getUser(user2).getPostits();
    Assert.assertEquals(1, postitsForUser1.size());
    Assert.assertEquals(1, postitsForUser2.size());

    Assert.assertEquals(getPostitService().getPostit(postitID1), postitsForUser1.get(0));
    Assert.assertEquals(getPostitService().getPostit(postitID2), postitsForUser2.get(0));
  }
}

