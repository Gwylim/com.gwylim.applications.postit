package com.gwylim.applications.postit.web.server.integration;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.web.server.shared.security.WebSecurityConfig;

/**
 * Test user navigation around the site.
 */
public class TestUserNaviagation extends WebTestBase {

  /** Check that created user appears in the database. */
  @Test
  public void testCreateUser() throws Exception {
    final String username = generateUsername();
    final String password = generatePassword();

    createUser(username, password);

    final User user = getUserService().getUser(username);
    Assert.assertEquals("Username does not match.", username, user.getUsername());
    Assert.assertTrue("Password does not match.", WebSecurityConfig.passwordEncoder().matches(password, user.getPassword()));
  }

  @Test
  public void testLoginBeforeCreate() throws Exception {
    final WebDriver driver = createDriver();

    final String username = generateUsername();
    final String password = generatePassword();

    driver.findElement(By.id("usernameInput")).sendKeys(username);
    driver.findElement(By.id("passwordInput")).sendKeys(password);
    driver.findElement(By.id("passwordInput")).submit();

    final WebElement error = driver.findElement(By.id("error"));
    Assert.assertEquals("Invalid username or password.", error.getText());

    // Check that we can login after failing.
    createUser(username, password);

    driver.findElement(By.id("usernameInput")).sendKeys(username);
    driver.findElement(By.id("passwordInput")).sendKeys(password);
    driver.findElement(By.id("passwordInput")).submit();

    checkLoggedIn(username, driver);
  }

  @Test
  public void testBadPassword() throws Exception {
    final String username = generateUsername();
    final String password = generatePassword();

    createUser(username, password);

    final WebDriver driver = loginUser(username, "notTheRightPassword");
    final WebElement error = driver.findElement(By.id("error"));
    Assert.assertEquals("Invalid username or password.", error.getText());

    // Check that we can login after failing.
    final WebDriver loggedIn = loginUser(username, password);
    checkLoggedIn(username, loggedIn);
  }

  @Test
  public void testSucessfulLogin() throws Exception {
    final String username = generateUsername();
    final WebDriver driver = createUserAndSignIn(username);
    checkLoggedIn(username, driver);
  }
}
