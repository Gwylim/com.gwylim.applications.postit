package com.gwylim.applications.postit.web.server.integration;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test following other users.
 */
public class TestFollow extends WebTestBase {

  private static final String USERNAME_1 = "user1";
  private static final String PASSWORD_1 = "password1";

  private static final String USERNAME_2 = "user2";
  private static final String PASSWORD_2 = "password2";

  @Test
  public void testAddRemoveFollow() throws UserCreationException, NoSuchUserException {
    createUserAndSignIn(USERNAME_1, PASSWORD_1);

    final WebDriver driver = createUserAndSignIn(USERNAME_2, PASSWORD_2);

    driver.findElement(By.id("friendName")).sendKeys(USERNAME_1);
    driver.findElement(By.id("friendName")).submit();

    Assert.assertEquals(1, driver.findElements(By.className("followedUsername")).size());
    Assert.assertEquals(USERNAME_1, driver.findElement(By.className("followedUsername")).getText());

    final User user1 = getUserService().getUser(USERNAME_1);
    Assert.assertEquals(0, user1.getFollowing().size());

    final User user2 = getUserService().getUser(USERNAME_2);
    Assert.assertEquals(1, user2.getFollowing().size());

    // Test unfollowing:

    // Find the unfollow-button and click it:
    driver.findElement(By.id("unfollow-" + user1.getUserID().toDB())).click();

    Assert.assertEquals(0, driver.findElements(By.className("followedUsername")).size());
  }


  @Test
  public void testFollowedPostits() throws UserCreationException {
    final WebDriver driver1 = createUserAndSignIn(USERNAME_1, PASSWORD_1);
    createPostit(USERNAME_1 + "-title1", "postitBody", driver1);
    createPostit(USERNAME_1 + "-title2", "postitBody", driver1);

    final WebDriver driver = createUserAndSignIn(USERNAME_2, PASSWORD_2);
    createPostit(USERNAME_2 + "-title1", "postitBody", driver);
    createPostit(USERNAME_2 + "-title2", "postitBody", driver);

    driver.findElement(By.id("friendName")).sendKeys(USERNAME_1);
    driver.findElement(By.id("friendName")).submit();


    final List<WebElement> postits = driver.findElement(By.id("followedPostits")).findElements(By.className("postitTitle"));
    Assert.assertEquals(2, postits.size());
    Assert.assertEquals(USERNAME_1 + "-title1", postits.get(0).getText());
    Assert.assertEquals(USERNAME_1 + "-title2", postits.get(1).getText());
  }

}
