package com.gwylim.applications.postit.web.server.integration;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.test.annotation.DirtiesContext;

import com.gwylim.applications.postit.web.server.basic.formData.CheckedData;
import com.gwylim.applications.postit.web.server.basic.formData.PostitData;

/**
 * Test postit creation at web level.
 */
@DirtiesContext
public class TestPostitCreation extends WebTestBase {

  @Test
  @DirtiesContext
  public void testSucessCreation() throws Exception {
    final String username = "test user";
    final String postitTitle = "Test postit title";
    final String postitBody = "Test postit body";

    final WebDriver driver = createUserAndSignIn(username);

    createPostit(postitTitle, postitBody, driver);

    final List<WebElement> titles = driver.findElements(By.className("postitTitle"));
    final List<WebElement> bodies = driver.findElements(By.className("postitBody"));

    Assert.assertEquals(1, titles.size());
    Assert.assertEquals(1, bodies.size());

    Assert.assertEquals(postitTitle, titles.get(0).getText());
    Assert.assertEquals(postitBody, bodies.get(0).getText());
  }


  @Test
  @DirtiesContext
  public void testCreationErrors() throws Exception {
    final String username = "test user";
    final WebDriver driver = createUserAndSignIn(username);

    final WebElement addPostit = driver.findElement(By.id("addPostit"));
    addPostit.click();


    // Check failing to enter any text:
    Assert.assertEquals("Create New PostIt", driver.getTitle());
    Assert.assertEquals("input", driver.findElement(By.id("title")).getTagName());
    Assert.assertEquals("textarea", driver.findElement(By.id("body")).getTagName());

    // Don't create any content.
//    titleInput.sendKeys(postitTitle);
//    bodyInput.sendKeys(postitBody);

    driver.findElement(By.id("body")).submit();

    Assert.assertEquals("Create New PostIt", driver.getTitle());
    Assert.assertEquals(PostitData.NO_CONTENT_ENTERED_ERROR, driver.findElement(By.id("titleError")).getText());
    Assert.assertEquals(PostitData.NO_CONTENT_ENTERED_ERROR, driver.findElement(By.id("bodyError")).getText());



    // Check entering too much text:
    Assert.assertEquals("Create New PostIt", driver.getTitle());
    for (int i = 0; i < CheckedData.MAX_FIELD_LENGTH + 1; i++) {
      driver.findElement(By.id("title")).sendKeys("x");
      driver.findElement(By.id("body")).sendKeys("X");
    }
    driver.findElement(By.id("body")).submit();

    Assert.assertEquals("Create New PostIt", driver.getTitle());
    Assert.assertEquals(PostitData.MAXIMUM_CHARACTERS_EXCEEDED_ERROR, driver.findElement(By.id("titleError")).getText());
    Assert.assertEquals(PostitData.MAXIMUM_CHARACTERS_EXCEEDED_ERROR, driver.findElement(By.id("bodyError")).getText());

    final String submittedTitleText = driver.findElement(By.id("title")).getAttribute("value");
    final String submittedBodyText = driver.findElement(By.id("body")).getText();
    assertMatches(submittedTitleText, "x{256}");
    assertMatches(submittedBodyText, "X{256}");




    // Check that we can still successfully submit even after the errors:
    driver.findElement(By.id("title")).clear(); // We should be able to do a single character delete here but it doesn't work
    driver.findElement(By.id("body")).clear(); // driver.findElement(By.id("xxx")).sendKeys(Keys.DELETE);
    driver.findElement(By.id("title")).sendKeys(submittedTitleText.substring(1));
    driver.findElement(By.id("body")).sendKeys(submittedBodyText.substring(1));

    driver.findElement(By.id("body")).submit();

    Assert.assertEquals("Front Page", driver.getTitle());

    final List<WebElement> titles = driver.findElements(By.className("postitTitle"));
    final List<WebElement> bodies = driver.findElements(By.className("postitBody"));

    Assert.assertEquals(1, titles.size());
    Assert.assertEquals(1, bodies.size());

    assertMatches(titles.get(0).getText(), "x{255}");
    assertMatches(bodies.get(0).getText(), "X{255}");
  }
}
