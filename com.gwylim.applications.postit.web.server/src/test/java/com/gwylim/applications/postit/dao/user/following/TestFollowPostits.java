package com.gwylim.applications.postit.dao.user.following;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test getting followed user postits.
 */
public class TestFollowPostits extends HibernareDAOTestBase {

  @Test
  public void testAddFollow() throws UserCreationException, NoSuchUserException {
    final User user1 = createUser();
    final User user2 = createUser();

    getUserService().follow(user1, user2);

    final Postit postit11 = getNewPostitService().newPostit("User 1's postit 1", "body", user1);
    final Postit postit12 = getNewPostitService().newPostit("User 1's postit 2", "body", user1);
    final Postit postit21 = getNewPostitService().newPostit("User 2's postit 1", "body", user2);
    final Postit postit22 = getNewPostitService().newPostit("User 2's postit 2", "body", user2);

    final List<Postit> postits2 = getPostitService().getFollowedPostits(user2);
    Assert.assertEquals(0, postits2.size());

    final List<Postit> postits1 = getPostitService().getFollowedPostits(user1);
    Assert.assertEquals(2, postits1.size());
    Assert.assertFalse("User1 unexpectedly sees their own postit in followed postit list:" + postits1, postits1.contains(postit11));
    Assert.assertFalse("User1 unexpectedly sees their own postit in followed postit list:" + postits1, postits1.contains(postit12));
    Assert.assertTrue("User1 does not see user2's postit 1 in their followed list:" + postits1,          postits1.contains(postit21));
    Assert.assertTrue("User1 does not see user2's postit 2 in their followed list:" + postits1,          postits1.contains(postit22));
  }
}
