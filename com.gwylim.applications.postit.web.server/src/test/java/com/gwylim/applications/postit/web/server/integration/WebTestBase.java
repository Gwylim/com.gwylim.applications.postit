package com.gwylim.applications.postit.web.server.integration;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;

/**
 * Base class for web tests.
 */
public abstract class WebTestBase extends HibernareDAOTestBase {

  @Autowired
  private EmbeddedWebApplicationContext _server;

  @Value("${local.server.port}")
  private Integer _port;

  /**
   * Test that the port has been set.
   */
  @Test
  public void metaTestCheckPort() {
    Assert.assertNotNull(_port);
  }

  /**
   * Create a postit with the given content given the driver is on the front page and signed in.
   * Will leave the driver on the front page with the new postit.
   * Checks that the postit has been successfully created.
   * @param postitTitle   The title of the new postit.
   * @param postitBody    The body of the new postit.
   * @param driver        The driver to use, must be on the front page an signed in.
   */
  public void createPostit(final String postitTitle, final String postitBody, final WebDriver driver) {
    final int intitalNumber = driver.findElements(By.className("postitTitle")).size();

    final WebElement addPostit = driver.findElement(By.id("addPostit"));
    addPostit.click();

    Assert.assertEquals("Create New PostIt", driver.getTitle());
    final WebElement titleInput = driver.findElement(By.id("title"));
    final WebElement bodyInput = driver.findElement(By.id("body"));

    Assert.assertEquals("input", titleInput.getTagName());
    Assert.assertEquals("textarea", bodyInput.getTagName());

    titleInput.sendKeys(postitTitle);
    bodyInput.sendKeys(postitBody);

    bodyInput.submit();

    final List<WebElement> titles = driver.findElements(By.className("postitTitle"));
    final List<WebElement> bodies = driver.findElements(By.className("postitBody"));

    Assert.assertEquals(intitalNumber + 1, titles.size());
    Assert.assertEquals(intitalNumber + 1, bodies.size());

    for (int i = 0; i < bodies.size(); i++) {
      if (postitTitle.equals(titles.get(i).getText()) && postitBody.equals(bodies.get(i).getText())) {
        return; // OK.
      }
    }

    Assert.fail("Could not find the created postit.");
  }

  /**
   * Create a user with the given username and sign in as them.
   * @param username  The user to create.
   * @return          A WebDriver on the front page, signed in as the given new user.
   */
  protected WebDriver createUserAndSignIn(final String username) {
    final String password = generatePassword();
    return createUserAndSignIn(username, password);
  }

  /**
   * Create a user with the given username and sign in as them.
   * @param username  The user to create.
   * @return          A WebDriver on the front page, signed in as the given new user.
   */
  protected WebDriver createUserAndSignIn(final String username, final String password) {
    createUser(username, password);
    return loginUser(username, password);
  }

  /**
   * Sign in as the given user.
   * @param username The user to sign in as.
   * @param password The password to sign using.
   * @return          A WebDriver on the front page, signed in as the given new user.
   */
  protected WebDriver loginUser(final String username, final String password) {
    final WebDriver driver = createDriver();

    final WebElement usernameInput = driver.findElement(By.id("usernameInput"));
    usernameInput.sendKeys(username);

    final WebElement passwordInput = driver.findElement(By.id("passwordInput"));
    passwordInput.sendKeys(password);
    passwordInput.submit();
    return driver;
  }

  /**
   * Check that the given user is logged in.
   */
  protected void checkLoggedIn(final String username, final WebDriver driver) {
    final WebElement usernameDisplay = driver.findElement(By.id("username"));
    Assert.assertEquals(usernameDisplay.getText(), username);
  }

  /**
   * Create the given user and leave them signed out.
   * @param username  The user to create.
   */
  protected void createUser(final String username, final String password) {
    final WebDriver driver = createDriver("SignUp");

    final WebElement usernameInput = driver.findElement(By.id("usernameInput"));
    usernameInput.sendKeys(username);

    final WebElement passwordInput = driver.findElement(By.id("passwordInput"));
    passwordInput.sendKeys(password);

    passwordInput.submit();
  }

  /**
   * Create a web driver at the front page of the site.
   * Will redirect to the sign in page if no user is signed in already.
   * @return  A new WebDriver.
   */
  protected WebDriver createDriver() {
    return createDriver("");
  }

  /**
   * Create a WebDriver at the given page.
   * @param page  The page to start at.
   * @return      The new WebDriver.
   */
  protected WebDriver createDriver(final String page) {
    final WebDriver driver = new HtmlUnitDriver();
    if (page.startsWith("/")) {
      driver.get(baseURL() + page);
    }
    else {
      driver.get(baseURL() + "/" + page);
    }
    return driver;
  }

  /**
   * Get the base page of the application.
   */
  protected String baseURL() {
    return "http://localhost:" + _port;
  }
}
