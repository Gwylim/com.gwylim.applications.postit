package com.gwylim.applications.postit.dao.postit;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.Postit;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test creation and reading of postits at DAO level.
 */
public class TestHibernarePostitDAOCreateRead extends HibernareDAOTestBase {

  @Test
  public void testAddReadPostit() throws UserCreationException, NoSuchUserException {
    final UserID userID = createUser().getUserID();

    final Postit postit = getNewPostitService().newPostit("title text", "body text", userID);

    Assert.assertNotNull("Newly added postit does not have an ID.", postit.getPostitID());

    final User user = getUserService().getUser(userID);
    final Postit gotPostit = getPostitService().getPostit(postit.getPostitID());

    final List<Postit> postits = user.getPostits();

    Assert.assertTrue(postits.contains(gotPostit));
    Assert.assertEquals("title text", gotPostit.getTitle());
    Assert.assertEquals("body text", gotPostit.getBody());
    Assert.assertNotNull(gotPostit.getPostitID());

    final Postit postit2 = getPostitService().getPostit(gotPostit.getPostitID());
    Assert.assertTrue(postits.contains(postit2));
    Assert.assertEquals("title text", postit2.getTitle());
    Assert.assertEquals("body text", postit2.getBody());
    Assert.assertNotNull(postit2.getPostitID());

    Assert.assertEquals(gotPostit, postit2);
  }

  @Test
  public void testAddForBadUser() throws UserCreationException {
    try {
      getNewPostitService().newPostit("title text", "body text", new UserID());
      Assert.fail("Expected exception not thrown.");
    }
    catch (final NoSuchUserException e) {
      // Expected.
    }
  }
}
