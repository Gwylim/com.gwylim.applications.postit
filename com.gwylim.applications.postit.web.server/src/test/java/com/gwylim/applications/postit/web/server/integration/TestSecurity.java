package com.gwylim.applications.postit.web.server.integration;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

/**
 * Test that a logged in user cannot access secured pages.
 */
public class TestSecurity extends WebTestBase {

  @Test
  public void testLoginFailure() throws Exception {
    final WebDriver front = createDriver();
    Assert.assertEquals("Sign In Page", front.getTitle());

    final WebDriver create = createDriver("CreatePostit");
    Assert.assertEquals("Sign In Page", create.getTitle());

    final WebDriver edit = createDriver("EditPostit?id=0");
    Assert.assertEquals("Sign In Page", edit.getTitle());
  }
}
