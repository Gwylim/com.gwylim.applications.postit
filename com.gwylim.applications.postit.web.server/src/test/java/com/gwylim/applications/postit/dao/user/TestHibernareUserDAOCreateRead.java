package com.gwylim.applications.postit.dao.user;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test creation and reading of Users at DAO level.
 */
public class TestHibernareUserDAOCreateRead extends HibernareDAOTestBase {

  @Test
  public void testAddUser() throws UserCreationException, NoSuchUserException {
    final String username = "user1";
    final UserID userID = getUserService().newUser(username, "testPassword");

    Assert.assertEquals(username, getUserService().getUser(userID).getUsername());
    Assert.assertEquals(userID, getUserService().getUser(userID).getUserID());

    Assert.assertEquals(username, getUserService().getUser(username).getUsername());
    Assert.assertEquals(userID, getUserService().getUser(username).getUserID());
  }

  @Test
  public void testAddDuplicateUser() throws UserCreationException {
    getUserService().newUser("user2", "testPassword1");
    try {
      getUserService().newUser("user2", "anything");
      Assert.fail("Expected UserCreationException to be thrown.");
    }
    catch (final UserCreationException e) {
      // Expected.
      Assert.assertEquals("User with name 'user2' already exists.", e.getMessage());
    }
  }
}
