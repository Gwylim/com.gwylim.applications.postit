package com.gwylim.applications.postit.web.server.integration;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.test.annotation.DirtiesContext;

/**
 * Test postit deletion at web level.
 */
@DirtiesContext
public class TestPostitDelete extends WebTestBase {

  @Test
  @DirtiesContext
  public void testDelete() throws Exception {
    final String username = "test user";
    final String postitTitle = "Test postit title";
    final String postitBody = "Test postit body";

    final WebDriver driver = createUserAndSignIn(username);

    createPostit(postitTitle, postitBody, driver);

    final WebElement delete = driver.findElement(By.className("delete"));
    delete.click();

    final List<WebElement> postits = driver.findElements(By.className("postitTitle"));
    Assert.assertEquals("Failed to delete postit.", 0, postits.size());
  }
}
