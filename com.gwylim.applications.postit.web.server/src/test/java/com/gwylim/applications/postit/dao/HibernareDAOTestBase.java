package com.gwylim.applications.postit.dao;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gwylim.applications.postit.Main;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NewPostitService;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.PostitService;
import com.gwylim.applications.postit.services.UserCreationException;
import com.gwylim.applications.postit.services.UserService;

/**
 * Base for testing wrt the Hibernate DAOs.
 */
@WebAppConfiguration
@IntegrationTest("server.port:0")
@DirtiesContext

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {HibernateConfiguration.class, Main.class, WebSecurityConfiguration.class})
@TestPropertySource(value = "classpath:test-application.properties")
public abstract class HibernareDAOTestBase {

  @Autowired
  private UserService _userService;

  @Autowired
  private PostitService _postitService;

  @Autowired
  private NewPostitService _newPostitService;

  public UserService getUserService() { return _userService; }
  public PostitService getPostitService() { return _postitService; }
  public NewPostitService getNewPostitService() { return _newPostitService; }

  /**
   * Create a new user, each will have a unique username.
   * May be called multiple times.
   * @return  The newly created user.
   * @throws NoSuchUserException when the creation fails so we cannot get it to return.
   */
  public User createUser() throws NoSuchUserException {
    try {
      final UserID newUser = getUserService().newUser(generateUsername(), generatePassword());
      Assert.assertNotNull(newUser);
      return getUserService().getUser(newUser);
    }
    catch (final UserCreationException e) {
      throw new RuntimeException("Failed to create a unique user.");
    }
  }

  private static final AtomicInteger UNIQUIFIER = new AtomicInteger();
  public String generatePassword() { return "testPassword" + UNIQUIFIER.getAndIncrement(); }
  public String generateUsername() { return "testUsername" + UNIQUIFIER.getAndIncrement(); }


  /**
   * Check that some text matches a regex.
   * @param text  The text to check.
   * @param regex The regex to check against.
   */
  public void assertMatches(final String text, final String regex) {
    Assert.assertTrue("'" + text + "' did not match '" + regex + "'.", text.matches(regex));
  }
}
