package com.gwylim.applications.postit.dao.user;

import org.junit.Assert;
import org.junit.Test;

import com.gwylim.applications.postit.dao.HibernareDAOTestBase;
import com.gwylim.applications.postit.model.User;
import com.gwylim.applications.postit.model.UserID;
import com.gwylim.applications.postit.services.NoSuchUserException;
import com.gwylim.applications.postit.services.UserCreationException;

/**
 * Test user update and delete at DAO level.
 */
public class TestHibernareUserDAOUpdateDelete extends HibernareDAOTestBase {

  @Test
  public void testChangeUsername() throws UserCreationException, NoSuchUserException {
    final String username = "testChangeUsername";
    final UserID userID = getUserService().newUser(username, "testChangeUsernamePassword");

    final User user = getUserService().getUser(userID);
    Assert.assertEquals(username, user.getUsername());
    Assert.assertNotNull(user.getUserID());

    final String newUsername = "NewUsername";
    getUserService().updateUsername(user, newUsername);

    final User userPostUpdate = getUserService().getUser(newUsername);
    Assert.assertNotNull("Failed to get user by username.", userPostUpdate);
    Assert.assertEquals(newUsername, userPostUpdate.getUsername());
    Assert.assertEquals(userID, userPostUpdate.getUserID());

    Assert.assertEquals(userPostUpdate, getUserService().getUser(userID));
  }

  @Test
  public void testDeleteUser() throws Exception {
    final String username = "testDeleteUser";
    final UserID userID = getUserService().newUser(username, "testDeleteUserPassword");

    final User user = getUserService().getUser(userID);
    Assert.assertEquals(username, user.getUsername());
    Assert.assertEquals(userID, user.getUserID());

    getUserService().delete(userID);

    try {
      getUserService().getUser(username);
      Assert.fail("Failed to delete user; still exists in username search.");
    }
    catch (final NoSuchUserException e) {
      // Expected.
    }

    try {
      getUserService().getUser(userID);
      Assert.fail("Failed to delete user; still exists in username search.");
    }
    catch (final NoSuchUserException e) {
      // Expected.
    }
  }
}
